// node , npm , npx are available once nodejs is installed

// command
// node --version (-v) // check version


// npm init  ==> it start a js project with creating package.json file
// npmjs.com ===> global repository that holds the pacakges/modules to be used in JS application

// npm install <pacakge_name>
// ==> it will update package.json file
// ===> it will create a lock file
// ====> all the installed pacakges are maitained inside node_modules folder

// to import nodejs inbuilt module and node_Modules folder's module we dont have to give path

// npm install < it will look after pacakge.json file's dependency section and install all the listed dependecy with its dependent packages'


// MVC
// models  (databse and database related stuff)
// views (user interface )
// controller(data preparation(validation,)) business logic

// 3 -tier
// presentation layer(view)
// data layer (model)
// application layer(controller)


// REST API 

// combination of REST and API

// API (Application programming interface)
// endpoint ==> combination of http method and url
// login API ==> POST server_address/login POST (http://localhost:8989/login)
// 


// REST Architecture (Representational state transfer)

// following points needs to addressed to be on REST
//1. stateless
// 2. correct use of http verb 
// GET ==> data fetch
// PUT/PATCH ==> data update
// POST ==> data submit(insert)
// DELETE ==> remove 
// PATCH
// url name
// 3. data must be either in JSON or XML
// 4. caching can be done for GET call (db call)


// email sending
// search product =={$operator , $in, $all,$lt}
// aggregation
