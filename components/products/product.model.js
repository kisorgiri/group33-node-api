const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RatingsSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    point: {
        type: Number,
        min: 1,
        max: 5
    },
    message: String
}, {
    timestamps: true
})

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    brand: String,
    category: {
        type: String,
        required: true
    },
    modelNo: String,
    price: Number,
    color: String,
    status: {
        type: String,
        enum: ['available', 'out-of-stock', 'booked'],
        default: 'available'
    },
    ratings: [RatingsSchema],
    size: String,
    images: [String],
    warrentyStatus: Boolean,
    warrentyPeroid: String,
    isReturnEligible: Boolean,
    isFeatured: Boolean,
    quantity: Number,
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            enum: ['percentage', 'quantity', 'value'],
        },
        discountValue: String
    },
    tags: [String],
    manuDate: Date,
    expiryDate: Date,
}, {
    timestamps: true
})

module.exports = mongoose.model('product', ProductSchema)

