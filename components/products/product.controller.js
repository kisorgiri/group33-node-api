const productQuery = require('./product.query')

function get(req, res, next) {
    var condition = {};
    if (req.user.role !== 1) {
        condition.vendor = req.user._id;
    }
    productQuery.find(condition)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })

}

function post(req, res, next) {
    const data = req.body;
    if (req.files) {
        data.images = req.files.map(function (file, index) {
            return file.filename;
        })
    }
    if (req.body.tags) {
        data.tags = (typeof (req.body.tags) === 'string')
            ? req.body.tags.split(',')
            : req.body.tags;
    }
    data.vendor = req.user._id;
    productQuery.insert(data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function getById(req, res, next) {
    const condition = {
        _id: req.params.id
    }
    productQuery.find(condition)
        .then(function (response) {
            if (!response[0]) {
                return next({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            res.json(response[0])
        })
        .catch(function (err) {
            next(err);
        })
}
function update(req, res, next) {
    const data = req.body;
    if (req.files.length > 0) {
        data.images = req.files.map(function (file, index) {
            return file.filename
        })
    }
    productQuery.update(req.params.id, data)
        .then(function (response) {
            // TODO remove old files if new files are there in a request
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function remove(req, res, next) {
    productQuery.remove(req.params.id)
        .then(function (response) {
            if (!response) {
                return next({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function search(req, res, next) {
    var searchCondition = {};
    console.log('req.body>>', req.body)
    if (req.body.category)
        searchCondition.category = req.body.category;
    if (req.body.name)
        searchCondition.name = req.body.name;
    if (req.body.brand)
        searchCondition.brand = req.body.brand;
    if (req.body.color)
        searchCondition.color = req.body.color;
    if (req.body.minPrice)
        searchCondition.price = {
            $gte: req.body.minPrice
        }

    if (req.body.maxPrice)
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    if (req.body.minPrice && req.body.maxPrice)
        searchCondition.price = {
            $lte: req.body.maxPrice,
            $gte: req.body.minPrice
        }

    if (req.body.fromDate && req.body.toDate) {
        const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0,);
        const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);

        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }
    if (req.body.tags) {
        searchCondition.tags = {
            $in: req.body.tags.split(',')
        }
    }
    console.log('search condition is >>', searchCondition)

    productQuery.find(searchCondition)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })

}
function addRatings(req, res, next) {
    const data = req.body;
    data.user = req.user._id;
    productQuery.addRatings(req.params.productId, data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}


module.exports = {
    get,
    getById,
    post,
    update,
    remove,
    search,
    addRatings
}

