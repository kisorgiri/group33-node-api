const ProductModel = require('./product.model');
function map_product_req(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.description)
        product.description = productDetails.description;
    if (productDetails.modelNo)
        product.modelNo = productDetails.modelNo;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.brand)
        product.brand = productDetails.brand;
    if (productDetails.quantity)
        product.quantity = productDetails.quantity;
    if (productDetails.color)
        product.color = productDetails.color;
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.size)
        product.size = productDetails.size;
    if (productDetails.images)
        product.images = productDetails.images;
    if (productDetails.warrentyStatus)
        product.warrentyStatus = productDetails.warrentyStatus;
    if (productDetails.warrentyPeroid)
        product.warrentyPeroid = productDetails.warrentyPeroid;
    if (productDetails.isReturnEligible)
        product.isReturnEligible = productDetails.isReturnEligible;
    if (productDetails.isFeatured)
        product.isFeatured = productDetails.isFeatured;
    if (productDetails.vendor)
        product.vendor = Object.keys(productDetails.vendor).length
            ? productDetails.vendor._id
            : productDetails.vendor;
    if (!product.discount)
        product.discount = {};
    if (productDetails.discountedItem)
        product.discount.discountedItem = productDetails.discountedItem;
    if (productDetails.discountType)
        product.discount.discountType = productDetails.discountType;
    if (productDetails.discountValue)
        product.discount.discountValue = productDetails.discountValue;
    if (productDetails.manuDate)
        product.manuDate = productDetails.manuDate;
    if (productDetails.expiryDate)
        product.expiryDate = productDetails.expiryDate;
    if (productDetails.tags)
        product.tags = typeof (productDetails.tags) === 'string'
            ? productDetails.tags.split(',')
            : productDetails.tags
}

function find(condition) {
    return ProductModel
        .find(condition)
        .sort({
            _id: -1
        })
        .populate('vendor', {
            username: 1,
            role: 1
        })
        .populate('ratings.user', {
            email: 1
        })
        .exec();
}

function insert(data) {
    var newProduct = new ProductModel({});
    map_product_req(newProduct, data);
    return newProduct.save()
}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            // if product now update

            map_product_req(product, data)
            product.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve(updated);
            })
        })
    })
}
function remove(id) {
    return ProductModel.findByIdAndRemove(id);
}

function addRatings(productId, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(productId, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            var ratings = {};
            if (data.ratingPoint)
                ratings.point = data.ratingPoint;

            if (data.ratingMessage)
                ratings.message = data.ratingMessage;

            if (data.user)
                ratings.user = data.user

            product.ratings.push(ratings);
            product.save(function (err, added) {
                if (err) {
                    return reject(err);
                }
                resolve(added)
            })
        })
    })
}

module.exports = {
    find,
    insert,
    update,
    remove,
    addRatings
}


