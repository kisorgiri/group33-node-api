const router = require('express').Router();
const proudctController = require('./product.controller');
const authenticate = require('./../../middlewares/authenticate');
const Uploader = require('./../../middlewares/uploader')

router.route('/')
    .get(authenticate, proudctController.get)
    .post(authenticate, Uploader.array('image'), proudctController.post);

router.route('/search')
    .get(proudctController.search)
    .post(proudctController.search);

router.route('/add-rating/:productId')
    .post(authenticate, proudctController.addRatings)

router.route('/:id')
    .get(authenticate, proudctController.getById)
    .put(authenticate, Uploader.array('image'), proudctController.update)
    .delete(authenticate, proudctController.remove);


module.exports = router;
