// database ==> database is a container(bucket) (memory allocation) where data are kept

// all the data operation (Create, Read, Update, Delete) are performed in a database

// Datbase Management System  is a programme that will provide a way to work with database
// eg of work
// structure, relation, indexing

// DBMS
// Relational DBMS
// NON-Relational (Distributed DBMS)

// 1 RDBMS
// a. Table based Design
// eg of LMS ==> books, Users, Reviews
// b. Schema based solution
// table's properties validation  
// eg books => id, name, author,title, editin, price
// c. row, tuple are records of table
// d.relation between table exists
// e. non scalable [Schema]
// f. SQL database (Structured Query Language)
// g. eg. mysql, sql-lite, postgres

// 2. Distributed Database Management
// a. Collection base approach
// eg of LMS ==> books, users, reviews
// b. Schema less design (no any schema validation)
// c. document are records inside collection (document based database)
// d. relation doesnot exist
// e. higly scalable
// f. No SQL (not only sql)
// g.eg mongodb ,redis, couchdb, cassendradb


// Shell Command

// show dbs ===> list all the available database
// use <db_name> // if(existing) select existing database else create and select new database
// db ==> selected database
// show collection ==> list all the collection available in selected db


// CRUD
// Create
// db.<collection_name>.insert({valid json})

// Read
// db.<collection_name>.find({query})
// db.<collection_name>.find({query}).count() // returns count
// db.<collection_name>.find({query}).pretty() // to format output
// db.<collection_name>.find(query).sort({
    // _id:-1
// })
// limit
// skip
// projection
// db.<collection_name>.find({query_builder},{projection_object})

// Update
// db.<collection_name>.update({},{},{});
// 1st object ==> query builder
// 2nd object==> it must have $set as key and [paylod to be updated] as value
// 3rd object is optional with options
// db.laptops.update({},{$set:{object_to_be_updated}},{multi:true, upsert:true})

// Delete
// db.<collection_name>.remove({query_builder});
// don't leave the query builder empty

// drop collection
// db.<selected_collection>.drop();

// drop database
// db.dropDatabase();


// tool to communicate with database
// tool is PL dependant
// ORM (Object Relation Mapping)  || ODM(Object Document Modelling)
// ORM ==> for relational database (SQL)
// ODM ==> for document based database(NOSQL)
// 

// mongoose is ODM for node and mongodb
// advantages of using ODM
// 1.Schema based solution
// 2.methods
// 3.datatype,
// 4.indexing is lot more easier
// 5.middleware


//##########DATABASE BACKUP & RESTORE######################
// command
// mongodump, mongorestore,mongoexport, mongoimport 

// two way==> machine(bson) , human(json and csv)
// 1.bson-----------------------------------------------------------
// back up 
// command mongodump
// usage
// mongodump ==> it will create backup of all the available database in a default dump folder
// mongodump --db <db_name> ==> it will create backup of selected database in default dump folder
// mongodump --db<db_name> --out <path_to_desired_folder>

// restore
// command mongorestore
// usage
// mongorestore ==> restore all the database available inside dump folder
// mongorestore --drop drop existing documents and restore 
// mongorestore <path_to_source_folder>
// bson----------------------------------------------------------

// 2. json and csv-------------------------------------------------
// backup command  mongoexport
// restore command mongoimport
// 2.1 json (backup)
// mongoexport --db<db_name> --collection <collection_name> --out <path_to_destination_with.csv extension>
// mongoexport -d<db_name> -c <collection_name> -o <path_to_destination_with.csv extension>
// restore
// mongoimport --db <new or existing db> --collection <new or existing collection> <path_to_source of json file>

// ===========================================================================================
// 2.2 csv
// backup 
// mongoexport --db <db_name> --collection <collection_name> --type=csv --fields 'comma,seperated,property_name' --out <path_to_destination_folder with.csv extension>
// mongoexport --db <db_name> --collection <collection_name> --type=csv --query='{"name":"value"}' --fields 'comma,seperated,property_name' --out <path_to_destination_folder with.csv extension>

// restore
// mongoimport  --db <db_name> --collection <collection_name> --type=csv <path_to_source file> --headerline

// 2. json and csv-------------------------------------------------
//##########DATABASE BACKUP & RESTORE######################

