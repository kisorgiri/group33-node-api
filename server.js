const http = require('http');

const server = http.createServer(function (req, res) {
    // req or 1st argument is always http request object
    // res or 2nd argument is always http response object
    // once client send request this callback will be executed
    console.log('request method >>', req.method);
    console.log('request url >>', req.url)
    console.log('client connected to server');

    // regardless of http method and http url this callback will be executed

    if(req.url ==='/write'){
        res.end('write work here');
    }
    else if (req.url ==='/read'){
        res.end('read work here');

    }else{
        res.end("nothing to perform")
    }
    // request response cycle must be completed

})

server.listen(7777, '127.0.0.1', function (err, done) {
    if (err) {
        console.log('error in listening');
    } else {
        console.log('server listening at port 7777 ');
        console.log('press CTRL + C to exit')
    }
});

