const fs = require('fs');


// fs.writeFile('./files/kishor.txt', 'i am happy to learn nodejs', function (err, done) {
//     if (err) {
//         console.log('file writing failed', err);
//     } else {
//         console.log('file writing success ', done)
//     }
// })
// const myWriteImported = require('./exec');

// //execution

// myWriteImported('test.js', 'hello world')
//     .then(function (data) {
//         console.log('success in write', data);
//     })
//     .catch(function (err) {
//         console.log('error is >>', err);
//     })

// read
fs.readFile('./files/kishor.txt', 'UTF-8', function (err, done) {
    if (err) {
        console.log('error reading >>', err);
    } else {
        console.log('success in reading >>', done);
    }
})

// todo 
// preare your own function to read
// handle result with callback or promise
// seperate function in another file and import

// rename
fs.rename('./files/kishor.txt', './files/broadway.txt', function (err, done) {
    if (err) {
        console.log('error in rename', err);
    } else {
        console.log('success in rename', done);
    }
})
// todo 
// preare your own function to read
// handle result with callback or promise
// seperate function in another file and import

//remove 
fs.unlink('./files/broadway.txt', function (err, done) {
    if (err) {
        console.log('remove failed');
    } else {
        console.log('remove success', done)
    }
})
