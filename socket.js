const socket = require('socket.io');
const config = require('./configs');
let users = [];

module.exports = function (app) {
    const io = socket(app.listen(config.SOCKET_PORT), {
        cors: {
            allow: '*'
        }
    });
    io.on('connection', function (client) {
        var id = client.id;
        console.log('Socket Client connected to server');
        // client.emit('welcome', 'welcome to socket server') // connected pathaune client lai
        client.broadcast.emit('welcome', 'for anothe client') //  // for every client connected to server except requesting client
        client.broadcast.to().emit(); // // for selected client (private communication)

        client.on('new-user', function (username) {
            users.push({
                id,
                username
            })
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
        client.on('new-message', function (data) {
            console.log('at new-message', data)
            client.emit('reply-message-own', data); // aafulai
            client.broadcast.to(data.receiverId).emit('reply-message-for', data) // aafu bahek arulai

        })
        client.on('disconnect', function () {
            users.forEach(function (user, index) {
                if (id === user.id) {
                    users.splice(index, 1);
                }
            })
            // client.emit('users', users);
            client.broadcast.emit('users', users)
        })
    })
}
