const express = require('express');
const morgan = require('morgan');
const app = express();
const path = require('path');
const config = require('./configs');
const cors = require('cors');

// events stuff
const events = require('events');
const myEvent = new events.EventEmitter();

myEvent.on('error', function (err, res) {
    console.log('at error listener', err);
    res.json(err)
})
app.use(function (req, res, next) {
    req.myEvent = myEvent;
    next();
})

// speration of concerns for socket
require('./socket.js')(app)

// import api route
const APIRoute = require('./routes/api.route');

require('./db_initilize');

// load third party middleware
app.use(morgan('dev'))
app.use(cors()); // accept all incoming request

// inbuilt middleware
app.use('/file', express.static(path.join(__dirname, 'uploads'))); // external serve

// NOTE all incoming request data must be parsed accordingly with their content type
// parser for x-www-formurlencoded
app.use(express.urlencoded({
    extended: true
}))

// json parser 
app.use(express.json());

//routing setup
app.use('/api', APIRoute)

// // 404 error catch middleware
app.use(function (req, res, next) {
    next({
        msg: 'Not Found',
        status: 404
    })
})

// Error Handling Middleware
// 1st arugment out of 4 is placeholder for error
// errorhandling middleware should be called (it will not came into action in between req-res-cycle)
// calling a next with arugment will execute error handling middleware
// 
app.use(function (err, req, res, next) {
    console.log('err s >>', err);
    // err is whatever sent in next call
    res.status(err.status || 400);
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})

app.listen(config.PORT, function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + config.PORT);
        console.log('press CTRL +C to exit')
    }
})


 // req or 1st argument is http request object
    // res or 2nd argument is http response object
    // following methods are used to end request response cycle with data
    // res.json(json data)
    // res.sendStatus(http_status_code)
    // res.send()
    // res.download()
    // res.sendFile();

    // express as independenat web framework
    // res.render()
    // res.redirect()

    // res.status() // it will not complete req -res cycle  but sets status code on response


// middleware 
// middleware is a function that has access to 
// http request object , http response object and next middleware function reference
// middleware can modify http request and response object
// middleware came into action in between request -response cycle
// middleware order is very very important

// syntax
// function(req,res,next){
//     // req or 1st arugment is always http request
//     // res or 2nd arugment is always http response object
//     // next or 3rd argument is next middleware function reference
// }

// middleware configuration
// app.use() is configuratio block for middleware

// types of middleware
// 1. application level middleware
// middleware having scope of http req, http res and next
    // app.use(function(req,res,next){
    //     // 
    // })
// 2.routing level middleware
// 
// 3.error handling middleware
// 4.thirdparty middleware
// 5.inbuilt middleware
