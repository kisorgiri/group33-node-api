const events = require('events');
const myEvent = new events.EventEmitter();
const myEvent1 = new events.EventEmitter();

// listens
myEvent1.on('random', function (data) {
    console.log('once random event is triggered,',data)
})

setTimeout(function(){
    // trigger
    myEvent.emit('random','ok')
},3000)
