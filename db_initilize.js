const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config')

const url = dbConfig.conxnURL + '/' + dbConfig.dbName;
mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
mongoose.connection.once('open', function () {
    console.log('db connection open');
})
mongoose.connection.on('err', function (err) {
    console.log('db connection failed')
})


