const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MAP_USER = require('./../helpers/map_user_req');

router.route('/')
    .get(function (req, res, next) {
        var condition = {};
        UserModel
            .find(condition)
            .sort({
                _id: -1
            })
            // .limit(10)

            // .skip()
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users);
            })
    })
    .post(function (req, res, next) {

    });


router.route('/search')
    .get(function (req, res, next) {
        res.send('from search user')
    })
    .post(function (req, res, next) {

    });

router.route('/change-password')
    .get(function (req, res, next) {
        res.send('from change password')
    });


router.route('/:id')
    .get(function (req, res, next) {
        UserModel.findOne({ _id: req.params.id }, function (err, user) {
            if (err) {
                return next(err)
            }
            if (!user) {
                return next({
                    msg: 'User not found',
                    status: 404
                })
            }
            res.json(user);
        })
    })
    .put(function (req, res, next) {
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }

            var updatedMappedUser = MAP_USER(user, req.body)

            updatedMappedUser.save()
                .then(function (data) {
                    res.json(data);
                })
                .catch(function (err) {
                    next(err);
                })

        })
    })
    .delete(function (req, res, next) {
        if (req.user.role !== 1) {
            return next({
                msg: 'you dont have access',
                status: 403
            })
        }
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User not found",
                    status: 404
                })
            }
            user.remove(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
    });



module.exports = router;
