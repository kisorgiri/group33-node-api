const express = require('express');
const UserModel = require('./../models/user.model');
const MAP_USER = require('./../helpers/map_user_req')
const router = express.Router();
const uploader = require('./../middlewares/uploader')
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const configs = require('../configs');
const nodemailer = require('nodemailer');

const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
})

function prepareEmail(data) {
    return {
        from: 'Group 33 Web Store', // sender address
        to: "salav.thapa4@gmail.com,kisorgiri@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<p><strong>Hello ${data.name}</strong>,</p>
        <p>We noticed that you are having trouble logging into our system.please click the link below to reset your password</p>
        <p><a href="${data.link}" target="_blank">click here to reset password</a></p>
        <p>Please contact system administrator for support</p>
        <p>Regards,</p>
        <p>Group 33 Store Suppor Team</p>
        `, // html body
    }
}

router.get('/', function (req, res, next) {
    require('fs').readFile('sdlk.sdlkfj', function (err, done) {
        if (err) {
            return req.myEvent.emit('error', err,res)
        }
    })
})

function createToken(user) {
    let token = jwt.sign({
        _id: user._id
    }, configs.JWT_SECRECT);

    return token;
}


router.post('/login', function (req, res, next) {
    UserModel
        .findOne({
            $or: [
                { email: req.body.username, },
                { username: req.body.username }
            ]
        })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "Invalid Username",
                    status: 400
                })
            }
            if (user.status !== 'active') {
                return next({
                    msg: 'Your account is disabled please contact system administrator for support',
                    status: 400
                })
            }
            var isMatched = passwordHash.verify(req.body.password, user.password);
            if (!isMatched) {
                return next({
                    msg: 'Invalid Password',
                    status: 400
                })
            }
            var token = createToken(user);
            res.json({
                user: user,
                token: token
            })
        })
        .catch(function (err) {
            next(err);
        })
})

router.post('/register', uploader.single('image'), function (req, res, next) {
    // post request data
    // db connection
    // db operation
    // req-res cycle must be completed
    console.log('req.body>>', req.body);
    console.log('req.file>>', req.file);

    if (req.fileTypeErr) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }

    if (req.file) {
        req.body.image = req.file.filename;
    }
    // data prepration
    const newUser = new UserModel(); //mongoose object
    // _id, __v property 
    // save (db_operation) methods

    var newMappedUser = MAP_USER(newUser, req.body)
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err); newMnewMappedUserappedUser
        }
        res.json(done);
    })
})

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Email Not Registered Yet!",
                    status: 404
                })
            }
            // if user found send email with reset link
            const emailContent = {
                name: user.username,
                email: user.email,
                link: `${req.headers.origin}/reset_password/${user._id}`
            }
            user.passwordExpiryTime = Date.now() + (1000 * 60 * 60 * 2);

            const emailBody = prepareEmail(emailContent);
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(emailBody, function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                })
            })

        })
})

router.post('/reset-password/:userId', function (req, res, next) {
    let userId = req.params.userId;
    UserModel.findOne({
        _id: userId,
        passwordExpiryTime: {
            $gte: Date.now()
        }
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: "Invalid/Expired Password Reset Token",
                status: 400
            })
        }
        user.password = passwordHash.generate(req.body.password);
        user.passwordExpiryTime = null;
        user.save(function (err, done) {
            if (err) {
                return next(err);
            }
            res.json(done)
        })
    })
})



module.exports = router;

