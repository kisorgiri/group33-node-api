const config = require('./../configs')
const jwt = require('jsonwebtoken');
const UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {
    let token;
    if (req.headers['x-access-token'])
        token = req.headers['x-access-token']
    if (req.headers['authorization'])
        token = req.headers['authorization']
    if (req.headers['token'])
        token = req.headers['token']
    if (req.query.token)
        token = req.query.token;

    if (!token) {
        return next({
            msg: 'Authentication Failed, Token Not Provided',
            status: 401
        })
    }

    // var a = token.split(' ')[1];
    jwt.verify(token, config.JWT_SECRECT, function (err, decoded) {
        if (err) {
            return next(err);
        }
        UserModel.findById(decoded._id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'Invalid/Expired Token',
                    status: 400
                })
            }
            req.user = user; // 
            next();
        })
    })
}

// prepare  a middleware for authorization
