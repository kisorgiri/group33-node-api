module.exports = function (req, res, next) {
    if (req.query.role === 'admin') {
        next();
    } else {
        next({
            msg: "You Dont Have Access",
            status: 403
        })
    }

}
