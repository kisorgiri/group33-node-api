const multer = require('multer');
const path = require('path');

// const upload = multer({
//     dest: 'uploads/'
// })

// disk storage
const myStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(process.cwd(), 'uploads/images'));
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
})

function typeFilter(req, file, cb) {
    // skip the file upload
    const mimeType = file.mimetype.split('/')[0];
    console.log('mime type is >>', mimeType);
    if (mimeType === 'image') {
        cb(null, true)
    } else {
        req.fileTypeErr = true;
        cb(null, false)
    }
}

const upload = multer({
    storage: myStorage,
    fileFilter: typeFilter
})

module.exports = upload;
