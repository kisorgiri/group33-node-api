module.exports = function (user, userDetails) {
    if (userDetails.name)
        user.name = userDetails.name;
    if (userDetails.email)
        user.email = userDetails.email
    if (userDetails.phoneNumber)
        user.phoneNumber = userDetails.phoneNumber
    if (userDetails.gender)
        user.gender = userDetails.gender;
    if (userDetails.dob)
        user.dob = userDetails.dob;
    if (userDetails.username)
        user.username = userDetails.username;
    if (userDetails.password)
        user.password = userDetails.password;
    if (!user.address)
        user.address = {}
    if (userDetails.tempAddress)
        user.address.tempAddress = userDetails.tempAddress.split(',')
    if (userDetails.permanentAddress)
        user.address.permanentAddress = userDetails.permanentAddress;
    if (userDetails.role)
        user.role = userDetails.role;
    if (userDetails.status)
        user.status = userDetails.status
    if (userDetails.image)
        user.image = userDetails.image

    return user;
}
