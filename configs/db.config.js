const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const conxnURL = 'mongodb://localhost:27017';
const dbName = 'group33db';
const OID = mongodb.ObjectID;

module.exports = {
    MongoClient: MongoClient,
    conxnURL: conxnURL,
    dbName: dbName,
    OID: OID
}
